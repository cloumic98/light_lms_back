
export interface StudentsDto{
    _id: string
    firstname: string
    lastname: string

    mail: string

    evaluation: {
        courseName: string,
        evaluationName: string
        note: number
    }[]

}