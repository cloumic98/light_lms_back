import axios from "axios";

export class CourseService{

    private static INSTANCE :CourseService
    private baseUrl: string = "http://localhost:3000"
    constructor() {
    }

    static get instance() {
        if(this.INSTANCE == null) {
            this.INSTANCE = new CourseService()
        }
        return this.INSTANCE
    }

    async getCourseStats(){
        return axios.get(this.baseUrl + "/courses/stats/course")
    }

    async getCourse(){
        return axios.get(this.baseUrl + "/courses")
    }
    async getOneCourse(courseId: string){
        return axios.get(this.baseUrl + "/courses/" + courseId)
    }

    async createCourse(data: any){
        return axios.post(this.baseUrl + '/courses', data )
    }

    async deleteCourse(courseId: string){
        return axios.delete(this.baseUrl + '/courses/' + courseId )
    }

    async updateCourse(courseData: any){
        return axios.put(this.baseUrl + '/courses/' + courseData._id , courseData)
    }

    async getCoursesWithFilters(filters: any){
        return axios.post(this.baseUrl + '/courses/filter', filters)
    }


}
