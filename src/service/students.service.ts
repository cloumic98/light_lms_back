import axios from "axios";


export class StudentsService{

    private static INSTANCE :StudentsService
    private baseUrl: string = "http://localhost:3000"



    constructor() {
    }

    static get instance() {
        if(this.INSTANCE == null) {
            this.INSTANCE = new StudentsService()
        }
        return this.INSTANCE
    }

    async getStudentStats(){
        return axios.get(this.baseUrl + "/students/stats/student")
    }

    async getStudent(){
        return axios.get(this.baseUrl + "/students")
    }
    async getOneStudent(studentId: string){
        return axios.get(this.baseUrl + "/students/" + studentId)
    }

    async createStudent(data: any){
        return axios.post(this.baseUrl + '/students', data )
    }

    async deleteStudent(studentId: string){
        return axios.delete(this.baseUrl + '/students/' + studentId )
    }

    async updateStudent(studentData: any){
        return axios.put(this.baseUrl + '/students/' + studentData._id , studentData)
    }
    async getStudentWithFilters(filters: any){
        return axios.post(this.baseUrl + '/students/filter', filters)

    }


}
