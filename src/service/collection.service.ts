import axios from "axios";

export class CollectionService{
    private static INSTANCE :CollectionService
    private baseUrl: string = "http://localhost:3000"

    static get instance() {
        if(this.INSTANCE == null) {
            this.INSTANCE = new CollectionService()
        }
        return this.INSTANCE
    }

    async getCollection(){
        return axios.get(this.baseUrl + '/collections')
    }
}