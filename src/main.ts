import {createApp, VueElement} from 'vue'
import './style.css'
import App from './App.vue'
import {createRouter, createWebHashHistory} from 'vue-router'
import BootstrapVue3 from 'bootstrap-vue-3'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue-3/dist/bootstrap-vue-3.css'
import student from "./components/student.vue";
import courses from "./components/courses.vue";
import studentInformation from "./components/studentInformation.vue";
import courseInformation from "./components/courseInformation.vue";

const routes = [
    {path: '/', component: courses },
    {path: '/students', component: student},
    {path: '/students/:id', component: studentInformation},
    {path: '/courses/:id', component: courseInformation}
]

const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHashHistory(),
    routes, // short for `routes: routes`
})
const Vue = createApp(App)

Vue.use(router)
Vue.use(BootstrapVue3)

Vue.mount('#app')
